## About
Get the candidate transcription factors to regulate differentially expressed genes for the lung alveolar (A549) cells infected by SARS-CoV2, and extract target genes by these transcription factors.

## Requirements
- zsh (v5.3)
- R (v4.0.3)
- [LAMP](https://github.com/a-terada/lamp) (Commit ID : 4bda2741)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% cd SourceCode
% git clone git@github.com:a-terada/lamp.git
% cd lamp
% make
```

## How to use
```sh
% cd SourceCode
% mkdir -p ../Result/TargetGeneList_A549/PlaneResult/ ../Result/TargetGeneList_A549/TruncatedResult
% mkdir -p ../Result/TargetGeneList_NHBE/PlaneResult/ ../Result/TargetGeneList_NHBE/TruncatedResult
% zsh main.sh
```
