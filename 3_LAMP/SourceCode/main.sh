# Copyright (c) 2021 Funahashi Lab., Keio University.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env bash
# For A549 ---------------------------
# Check the file is no problem or not
python ./lamp/check_files.py ../SourceData/item/itemDataForLAMP_A549.csv ../SourceData/value/ValueDataForLAMP_A549.csv
python ./lamp/check_files.py ../SourceData/item/itemDataTruncatedForLAMP_A549.csv ../SourceData/value/ValueDataForLAMP_A549.csv

# LAMP Analysis was executed 
python ./lamp/lamp.py -p fisher ../SourceData/item/itemDataForLAMP_A549.csv ../SourceData/value/ValueDataForLAMP_A549.csv 0.05 > ../Result/Result_A549.txt
python ./lamp/lamp.py -p fisher ../SourceData/item/itemDataTruncatedForLAMP_A549.csv ../SourceData/value/ValueDataForLAMP_A549.csv 0.05 > ../Result/ResultTruncated_A549.txt
python ./lamp/eliminate_comb.py ../Result/Result_A549.txt > ../Result/Result_Merged_A549.txt
python ./lamp/eliminate_comb.py ../Result/ResultTruncated_A549.txt > ../Result/ResultTruncated_Merged_A549.txt

# Extract DEG of target genes by transcription factors detected as enriched
R --vanilla --slave --args 'A549' < extractDEGofTF.R

# For NHBE ---------------------------
# Check the file is no problem or not
python ./lamp/check_files.py ../SourceData/item/itemDataForLAMP_NHBE.csv ../SourceData/value/ValueDataForLAMP_NHBE.csv
python ./lamp/check_files.py ../SourceData/item/itemDataTruncatedForLAMP_NHBE.csv ../SourceData/value/ValueDataForLAMP_NHBE.csv

# LAMP Analysis was executed 
python ./lamp/lamp.py -p fisher ../SourceData/item/itemDataForLAMP_NHBE.csv ../SourceData/value/ValueDataForLAMP_NHBE.csv 0.05 > ../Result/Result_NHBE.txt
python ./lamp/lamp.py -p fisher ../SourceData/item/itemDataTruncatedForLAMP_NHBE.csv ../SourceData/value/ValueDataForLAMP_NHBE.csv 0.05 > ../Result/ResultTruncated_NHBE.txt
python ./lamp/eliminate_comb.py ../Result/Result_NHBE.txt > ../Result/Result_Merged_NHBE.txt
python ./lamp/eliminate_comb.py ../Result/ResultTruncated_NHBE.txt > ../Result/ResultTruncated_Merged_NHBE.txt

# Extract DEG of target genes by transcription factors detected as enriched
R --vanilla --slave --args 'NHBE' < extractDEGofTF.R
