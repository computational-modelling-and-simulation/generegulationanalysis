# Gene regulation inference of COVID-19

================

Akira Funahashi: <funa@bio.keio.ac.jp>;

Date: 13/04/2021

## Introduction

The current disease map lacks the gene regulatory relationships, though it is critically related to the outcome of COVID-19. Therefore, we will try to infer gene regulation from responded genes against COVID-19 and public binding information of the human transcription factor. This information of gene regulation will contribute to the elucidation of drug targets focusing on the relationships between transcription factors to be related to important gene regulation and molecules in the COVID-19 disease map.

## License
Copyright (c) 2021 Funahashi Lab., Keio University.

Modifications Copyright (c) 2020, Alberto Valdeolivas ( for 1_DEGAnalysis/SourceCode/DEGAnalysis.R)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## All code and how to use
### 1_DEGAnalysis

Extract differentially expressed genes between lung alveolar (A549) cells before and after infection of SARS-CoV2 (GSE147507, https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE147507).
All of the codes were basically same as in https://github.com/saezlab/Covid19_phospho.

#### Requirements
- R (v4.0.3)
- [DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html) (v1.30.0)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% R
> if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
> BiocManager::install("DESeq2")
```

#### How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("DEGAnalysis.R")
> TARGET_SAMPLE <- "NHBE"; source("DEGAnalysis.R")
```

### 2_ExtractItemData

Get the gene regulatory relathionships converted into item file of LAMP (https://github.com/a-terada/lamp) in DoRoThea (https://saezlab.github.io/dorothea/).

#### Requirements
- R (v4.0.3)
- [dorothea](http://bioconductor.org/packages/release/data/experiment/html/dorothea.html) (v1.2.1)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% R
> if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
> BiocManager::install("dorothea")
```

#### How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("main.R")
> TARGET_SAMPLE <- "NHBE"; source("main.R")
```
### 3_LAMP
Get the candidate transcription factors to regulate differentially expressed genes for the lung alveolar (A549) cells infected by SARS-CoV2, and extract target genes by these transcription factors.

#### Requirements
- zsh (v5.3)
- R (v4.0.3)
- [LAMP](https://github.com/a-terada/lamp) (Commit ID : 4bda2741)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% cd SourceCode
% git clone git@github.com:a-terada/lamp.git
% cd lamp
% make
```

#### How to use
```sh
% cd SourceCode
% mkdir -p ../Result/TargetGeneList_A549/PlaneResult/ ../Result/TargetGeneList_A549/TruncatedResult
% mkdir -p ../Result/TargetGeneList_NHBE/PlaneResult/ ../Result/TargetGeneList_NHBE/TruncatedResult
% zsh main.sh
```


### 4_GOEA
Perform GO Enrichment analysis for DEGs targeted by transcription factors detected as enriched by LAMP.

#### Requirements
- R (v4.0.3)
- [GSEABase](https://bioconductor.org/packages/release/bioc/html/GSEABase.html) (v1.52.1)
- [GOstats](https://bioconductor.org/packages/release/bioc/html/GOstats.html) (v2.56.0)
- [seqinr](https://cran.r-project.org/web/packages/seqinr/index.html) (v4.2-5)
- [GO.db](https://bioconductor.org/packages/release/data/annotation/html/GO.db.html) (v3.12.1)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% R
> if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
> BiocManager::install("GSEABase")
> BiocManager::install("GOstats")
> BiocManager::install("GO.db")
> install.packages("seqinr")
```

#### How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("go_enrichment.R")
> source("add_DEG_type_info.R")
> TARGET_SAMPLE <- "NHBE"; source("go_enrichment.R")
> source("add_DEG_type_info.R")
```


### 5_SBMLSearch
Grep HGNC symbol (detected TF in LAMP) from Disease map files.

#### How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("grep_disease_map.R")
> TARGET_SAMPLE <- "NHBE"; source("grep_disease_map.R")
```

### 6_DrugBank
Grep DRUGBANK ID in .xlsx file from DRUGBANK HTML files under html/ directory.
New .xlsx file (foo-new.xlsx) which includes the grep results will be generated.

#### Requirements
- Python 3.6+
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
- [openpyxl](https://openpyxl.readthedocs.io/en/stable/)

For pip (with virtualenv):
```sh
% python -m venv venv
% source ./venv/bin/activate
% pip install --upgrade pip
% pip install beautifulsoup4
% pip install openpyxl
```

For [MacPorts](https://www.macports.org/) users:
```sh
% sudo port install python38
% sudo port install py38-beautifulsoup4
% sudo port install py38-openpyxl
```

#### How to use
```sh
% cd 6_DrugBank/SourceCode
% python grepDBID.py ../SourceData/Summary_addmed.xlsx ../SourceData/html/
% mv Summary_addmed-new.xlsx ../Result
```

### 7_DrugEnrichmentAnalysis
Extract the enriched target drugs based on INDRA and AILANI database. (See the data in https://gitlab.lcsb.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/harmonised_drugs_mirnas_ups.tsv)

#### Requirements
- R (v4.1.2)
- [openxlsx](https://cran.r-project.org/web/packages/openxlsx/index.html) (v4.2.5)

#### How to use
```sh
% cd 7_DrugEnrichmentAnalysis/SourceCode
% Rscript drug_enrichment_analysis.R
```

#### CAUTION
Currently, the above program does not work because the files under html are excluded for licensing reasons.

## Results
All of the results generated by the codes were stored in Result directory in each analysis directory.
Summary of the results were stored as Summary.xlsx in FinalResult.
The contents of Summary.xlsx was following,

- 1st sheet : List of the candidate TFs to regulate DEGs in the affection of COVID19 (3_LAMP/Result/Result.txt)

- 2nd sheet : Enriched GO for target genes by each TF (Biological Process, Fisher's exact test , p.value < 0.05)

- 3rd sheet : Enriched GO for target genes by each TF (Cellular Component, Fisher's exact test , p.value < 0.05)

- 4th sheet : Enriched GO for target genes by each TF (Molecular Function, Fisher's exact test , p.value < 0.05)

- 5th sheet : List of drugs to target the candidate TFs detected by LAMP analysis (mainly manually created by Prof. Noriko F. Hiroi using the [COVID19 Dashboard in DrugBank](https://go.drugbank.com/covid-19). All of the contents were manually curated by all of the members in this project.


