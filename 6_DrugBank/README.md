## About
Grep DRUGBANK ID in .xlsx file from DRUGBANK HTML files under html/ directory.
New .xlsx file (foo-new.xlsx) which includes the grep results will be generated.

## Requirements
- Python 3.6+
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
- [openpyxl](https://openpyxl.readthedocs.io/en/stable/)

For pip (with virtualenv):
```sh
% python -m venv venv
% source ./venv/bin/activate
% pip install --upgrade pip
% pip install beautifulsoup4
% pip install openpyxl
```

For [MacPorts](https://www.macports.org/) users:
```sh
% sudo port install python38
% sudo port install py38-beautifulsoup4
% sudo port install py38-openpyxl
```

## How to use
```sh
% cd SourceCode
% python grepDBID.py ../SourceData/Summary_addmed.xlsx ../SourceData/html/
% mv Summary_addmed-new.xlsx ../Result
```
