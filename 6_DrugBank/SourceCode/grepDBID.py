## Copyright (c) 2021 Funahashi Lab., Keio University.

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

# parse HTML
from bs4 import BeautifulSoup

# .xlsx support
import openpyxl

import glob
import os
import re
import sys


def parseHTML(htmlfile, dbids):
    soup = BeautifulSoup(open(htmlfile), "html.parser")

    table = soup.find("table", id="DataTables_Table_2")
    tbody = table.find("tbody")
    rows = tbody.find_all("tr")

    for row in rows:
        cells = row.find_all("td")
        cell = cells[1]
        a = cell.find("a")
        s = a["href"].replace("https://go.drugbank.com/drugs/", "")
        dbids.add(s)


def resultXLSX(xlsxfile, dbids):
    target_row = "drug Name[DRUGBANK Accession Number]"

    wb = openpyxl.load_workbook(xlsxfile)
    ws = wb["Drugs"]

    ## Create a dictionary of column names
    colnames = {}
    current = 0
    for col in ws.iter_cols(1, ws.max_column):
        colnames[col[0].value] = current
        current += 1

    ws.insert_cols(colnames[target_row] + 2)
    ws.cell(
        row=1, column=colnames[target_row] + 2
    ).value = "External Clinical Trials for COVID-19 and Related Conditions?"

    for row_cells in ws.iter_rows(min_row=2):
        drugname = row_cells[colnames[target_row]].value
        if drugname != None:
            m = re.match(r".+(DB[0-9]+).+", drugname)
            if m != None:
                query = m.group(1)
                if query in dbids:
                    row_cells[colnames[target_row] + 1].value = 1
                    print("1", drugname)
                else:
                    row_cells[colnames[target_row] + 1].value = 0
                    print("0", drugname)

    newfile = os.path.basename(xlsxfile).replace(".xlsx", "")
    wb.save(filename=newfile + "-new.xlsx")


def usage():
    s = """Usage: python grepDBID.py foo.xlsx html_dir

    Grep DRUGBANK ID in .xlsx file from DRUGBANK HTML files under html/ directory.
    New .xlsx file (foo-new.xlsx) which includes the grep results will be generated.

    (ex.) python grepDBID.py ../SourceData/Summary_addmed.xlsx ../SourceData/html/"""
    print(s)
    exit(0)


def main():
    if (
        len(sys.argv) < 3
        or not os.path.isfile(sys.argv[1])
        or not os.path.isdir(sys.argv[2])
    ):
        usage()

    dbids = set({})
    xlsxfile = sys.argv[1]

    for htmlfile in glob.glob(sys.argv[2] + "/*.html"):
        parseHTML(htmlfile, dbids)

    resultXLSX(xlsxfile, dbids)


if __name__ == "__main__":
    main()
