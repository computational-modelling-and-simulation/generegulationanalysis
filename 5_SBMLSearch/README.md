## About
Grep HGNC symbol (detected TF in LAMP) from Disease map files.

## How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("grep_disease_map.R")
> TARGET_SAMPLE <- "NHBE"; source("grep_disease_map.R")
```
