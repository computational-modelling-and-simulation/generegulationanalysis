## Copyright (c) 2021 Funahashi Lab., Keio University.

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import argparse
import libsbml
import os

parser = argparse.ArgumentParser(description="Grep species name from SBML files.")
parser.add_argument("-n", "--name", default="E2F4", help="Species name")
parser.add_argument("inputfiles", help="SBML file to process", type=str, nargs="*")

args = parser.parse_args()

# print(args.inputfiles)

for sbmlfile in args.inputfiles:
    d = libsbml.readSBMLFromFile(sbmlfile)
    m = d.getModel()
    for i in range(m.getNumSpecies()):
        s = m.getSpecies(i)
        if s.getName().lower() == args.name.lower():
            print(args.name + ":" + sbmlfile)
