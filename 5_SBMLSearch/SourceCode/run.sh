## Copyright (c) 2021 Funahashi Lab., Keio University.

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env zsh
touch ../Result/result.txt
foreach i (`cat ../SourceData/lamp_statistics.csv | cut -d',' -f4`)
python grepSBML.py --name $i ../Sourcedata/models/**/*.xml >> ../Result/result.txt
end
