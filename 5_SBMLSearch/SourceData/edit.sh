#!/usr/bin/env bash
sed s/","/";"/g ../../3_LAMP/Result/Result_Merged_A549.txt > tmp_Result_Merged_A549_comma_replaced.txt
sed s/","/";"/g ../../3_LAMP/Result/Result_Merged_NHBE.txt > tmp_Result_Merged_NHBE_comma_replaced.txt
awk 'BEGIN{OFS=","}{$1=$1;if($1 != "\#" && $1 != "Rank" && $1 != "Time") print $0}' tmp_Result_Merged_A549_comma_replaced.txt > lamp_statistics_A549.csv
awk 'BEGIN{OFS=","}{$1=$1;if($1 != "\#" && $1 != "Rank" && $1 != "Time") print $0}' tmp_Result_Merged_NHBE_comma_replaced.txt > lamp_statistics_NHBE.csv
rm tmp_Result_Merged_*
