# Disease map file (COVID19_Disease_Map_bipartite_crosslinked_additional_HGNCs.tsv)
Download from the following repository:
https://gitlab.lcsb.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/COVID19_Disease_Map_bipartite_crosslinked_additional_HGNCs.tsv

# LAMP result files (lamp_statistics_A549.csv, lamp_statistics_NHBE.csv)
Run the following command
```
% ./edit.sh
```
