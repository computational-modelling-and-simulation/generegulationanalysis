## Copyright (c) 2021 Funahashi Lab., Keio University.

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

##################################################
## Project:
## Script purpose: GO Enrichment analysis for DEGs targeted by transcription factors detected as enriched
## Date: Sat Feb 20 19:59:52 2021
## Author: Takahiro G Yamada and Yusuke Hiki
## Dependency: GOstats(=2.56.0), GO.db(=3.12.1)
##################################################
# TARGET_SAMPLE <- commandArgs()[ length(commandArgs()) ]

## Installed Library
library( GSEABase )
library( GOstats )
library( seqinr )
## Options
options( stringsAsFactors = F)

## Data Installation
TargetedGeneList <- list.files(paste("../SourceData/TargetGeneList_", TARGET_SAMPLE, "/PlaneResult", sep="") , full.names = T )
rankTF <- sapply( strsplit( TargetedGeneList , "/") , FUN = function(x){return(x[5])})
rankTF <- sapply( strsplit( rankTF , "_") , FUN = function(x){return(paste(x[1],"_",x[2],sep =""))})

TargetedGeneList_Truncated <- list.files(paste("../SourceData/TargetGeneList_", TARGET_SAMPLE, "/TruncatedResult", sep="") , full.names = T )
rankTF_Truncated <- sapply( strsplit( TargetedGeneList_Truncated , "/") , FUN = function(x){return(x[5])})
rankTF_Truncated <- sapply( strsplit( rankTF_Truncated , "_") , FUN = function(x){return(paste(x[1],"_",x[2],sep =""))})


GOAnnot <- read.table("../SourceData/GOList/goList.txt" , sep = "\t" , header = T)

goFrame <- GOFrame( GOAnnot, organism = "Homo sapiens")
goAllFrame <- GOAllFrame( goFrame )

gsc <- GeneSetCollection( goAllFrame , setType = GOCollection())

all <- unique( GOAnnot$frame.gene_id )

print( "GO Enrichment Analysis for Plain Result...")
for( i in 1:length( TargetedGeneList )){
  print(paste( rankTF[ i ] , " is processing..." , sep = ""))
  targetGene <- read.table( TargetedGeneList[ i ])[,1]
  p_BP <- GSEAGOHyperGParams(
           name = "Param",
           geneSetCollection = gsc,
           geneIds = targetGene,
           universeGeneIds = all,
           ontology = "BP",
           pvalueCutoff = 0.05,
           conditional = FALSE,
           testDirection = "over"
  )
  p_MF <- GSEAGOHyperGParams(
    name = "Param",
    geneSetCollection = gsc,
    geneIds = targetGene,
    universeGeneIds = all,
    ontology = "MF",
    pvalueCutoff = 0.05,
    conditional = FALSE,
    testDirection = "over"
  )
  p_CC <- GSEAGOHyperGParams(
    name = "Param",
    geneSetCollection = gsc,
    geneIds = targetGene,
    universeGeneIds = all,
    ontology = "CC",
    pvalueCutoff = 0.05,
    conditional = FALSE,
    testDirection = "over"
  )
  result_BP <- hyperGTest( p_BP )
  result_MF <- hyperGTest( p_MF )
  result_CC <- hyperGTest( p_CC )
  
  summary_BP <- summary( result_BP )
  summary_MF <- summary( result_MF )
  summary_CC <- summary( result_CC )
  
  # Filtering out the count == 1
  summary_BP <- summary_BP[ summary_BP$Count != 1 , ]
  summary_MF <- summary_MF[ summary_MF$Count != 1 , ]
  summary_CC <- summary_CC[ summary_CC$Count != 1 , ]
  
  # Sort based on Odds Ratio
  summary_BP <- summary_BP[ order( summary_BP$OddsRatio , decreasing = T) , ]
  summary_MF <- summary_MF[ order( summary_MF$OddsRatio , decreasing = T) , ]
  summary_CC <- summary_CC[ order( summary_CC$OddsRatio , decreasing = T) , ]
  
  if( !(dir.exists(paste( "../Result_", TARGET_SAMPLE, "/PlaneResult/" , rankTF[ i ] , sep = "")))){
    dir.create(paste("../Result_", TARGET_SAMPLE, "/PlaneResult/" , rankTF[ i ] , sep = ""), recursive=T)
  }
  write.table( summary_BP , paste("../Result_", TARGET_SAMPLE, "/PlaneResult/" , rankTF[ i ] , "/EnrichedGO_BiologicalProcess.tsv" , sep = "") , row.names = F , quote = F, sep = "\t")
  write.table( summary_MF , paste("../Result_", TARGET_SAMPLE, "/PlaneResult/" , rankTF[ i ] , "/EnrichedGO_MolecularFunction.tsv" , sep = "") , row.names = F , quote = F , sep = "\t")
  write.table( summary_CC , paste("../Result_", TARGET_SAMPLE, "/PlaneResult/" , rankTF[ i ] , "/EnrichedGO_CellularComponent.tsv" , sep = "") , row.names = F , quote = F , sep = "\t")
  
  tmpGOList <- vector()
  tmpGeneList <- vector()
  
  for( j in 1:length( result_BP@goDag@nodeData@data ) ){
    tmpGO <- names( result_BP@goDag@nodeData@data[ j ] )
    for( k in result_BP@goDag@nodeData@data[[ j ]]$geneIds ){
      tmpGOList[ length( tmpGOList ) + 1 ] <- tmpGO
      tmpGeneList[ length( tmpGeneList ) + 1] <- k
    }
  }
  
  for( j in 1:length( result_MF@goDag@nodeData@data ) ){
    tmpGO <- names( result_MF@goDag@nodeData@data[ j ] )
    for( k in result_MF@goDag@nodeData@data[[ j ]]$geneIds ){
      tmpGOList[ length( tmpGOList ) + 1 ] <- tmpGO
      tmpGeneList[ length( tmpGeneList ) + 1] <- k
    }
  }
  
  for( j in 1:length( result_CC@goDag@nodeData@data ) ){
    tmpGO <- names( result_CC@goDag@nodeData@data[ j ] )
    for( k in result_CC@goDag@nodeData@data[[ j ]]$geneIds ){
      tmpGOList[ length( tmpGOList ) + 1 ] <- tmpGO
      tmpGeneList[ length( tmpGeneList ) + 1] <- k
    }
  }
  
  GOGeneList <- cbind( tmpGOList , tmpGeneList )
  colnames( GOGeneList ) <- c("GO" , "Gene")
  write.csv( GOGeneList , paste("../Result_", TARGET_SAMPLE, "/PlaneResult/" , rankTF[ i ] , "/GOGeneList.csv" , sep = "") , row.names = F , quote = F)
  
  print(paste( rankTF[ i ], " is finished." , sep = ""))
}

