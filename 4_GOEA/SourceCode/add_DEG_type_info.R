## Copyright (c) 2021 Funahashi Lab., Keio University.

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.


##########################################
# Project: Add DEG regulation types information to GOEA result
# Date: 2021/02/22
# Author:Yusuke Hiki
##########################################
options( stringsAsFactors=F )

# Function to add directory
addir <- function(outputdir){
  if( !file.exists(outputdir) ){
    dir.create(outputdir)
    print( paste("Generated:", outputdir) )
  }
}

# Add output directory
output_dir <- paste( "../../4_GOEA/Result_", TARGET_SAMPLE, "/PlaneResult_RegulationTypeInfoAdded/", sep="" )
addir( output_dir )

# Get DEG type (up- or down-regulated)
deg_result <- read.csv( paste( "../../1_DEGAnalysis/Result/DEGAnalysisResultWithDESeq2_", TARGET_SAMPLE, ".csv", sep="" ), row.names=1 )
deg_result <- na.exclude( deg_result )
deg_result <- deg_result[ deg_result$padj<0.05, ]
deg_type <- rep( "down", nrow(deg_result) )
deg_type[ deg_result$log2FoldChange>0 ] <- "up"
names( deg_type ) <- row.names( deg_result )

# Get input files & output dirs
rank_and_tf <- list.dirs( paste( "../../4_GOEA/Result_", TARGET_SAMPLE, "/PlaneResult", sep="" ), full.names=F )[ -1 ]
input_files <- list.files( paste( "../../4_GOEA/Result_", TARGET_SAMPLE, "/PlaneResult/", rank_and_tf, sep="" ), full.names=T )
output_dirs <- paste( "../../4_GOEA/Result_", TARGET_SAMPLE, "/PlaneResult_RegulationTypeInfoAdded/", rank_and_tf, "/", sep="" )

errors <- c()
for( i_tf in 1:length( rank_and_tf ) ){
  # Get input file, output directory path, and regulated DEGs by target TF
  rank_and_tf_i <- rank_and_tf[ i_tf ]
  input_files_i <- input_files[ grep( rank_and_tf_i, input_files ) ]
  output_dir_i <- output_dirs[ i_tf ]
  addir( output_dir_i )
  regulated_deg_i <- c( read.table( paste( "../../3_LAMP/Result/TargetGeneList_", TARGET_SAMPLE, "/PlaneResult/", rank_and_tf_i, "_targetDEG.txt", sep="" ) )[,1] )

  # Processing call
  print( paste( "Processing : ", rank_and_tf_i, " ( ", i_tf, " / ", length( rank_and_tf ), " )", sep="" ) )

  goea_result_BP_i <- read.table( input_files_i[1], sep="\t", header=T, quote="" )
  goea_result_CC_i <- read.table( input_files_i[2], sep="\t", header=T, quote="" )
  goea_result_MF_i <- read.table( input_files_i[3], sep="\t", header=T, quote="" )
  colnames( goea_result_BP_i )[1] <- colnames( goea_result_CC_i )[1] <- colnames( goea_result_MF_i )[1] <- "GOID"
  goea_result_i <- rbind( goea_result_BP_i, goea_result_CC_i, goea_result_MF_i )
  
  GOGeneList <- read.csv( paste( "../../4_GOEA/Result_", TARGET_SAMPLE, "/PlaneResult/", rank_and_tf_i, "/GOGeneList.csv", sep="" ), header=T )
  
  # For each enriched GO
  added_info_i <- c()
  errors <- c()
  for( j_go in 1:length( goea_result_i$GOID ) ){
    go_j <- goea_result_i$GOID[ j_go ]
    regulated_deg_with_target_go_ij <- intersect( regulated_deg_i, GOGeneList$Gene[ GOGeneList$GO == go_j ] )
    
    if( goea_result_i$Count[ j_go ] != length( regulated_deg_with_target_go_ij ) ){
      print( paste( j_go, " : ", go_j, " has something wrong...", sep="" ) )
      errors <- append( errors, go_j )
    }
    deg_type_ij <- deg_type[ regulated_deg_with_target_go_ij ]  # Get DEG type of regulated DEGs with target GO
    
    # Get DEG type information to be added
    added_info_ij <- data.frame( UpCount=length( grep( "up", deg_type_ij ) ),
                                 DownCount=length( grep( "down", deg_type_ij ) ) )
    added_info_i <- rbind( added_info_i, added_info_ij )
    row.names( added_info_i )[ nrow( added_info_i ) ] <- go_j
  }
  
  # Add extracted information
  goea_result_BP_i <- cbind( goea_result_BP_i,
                             added_info_i[ match( goea_result_BP_i$GOID, row.names( added_info_i ) ), ] )
  goea_result_CC_i <- cbind( goea_result_CC_i,
                             added_info_i[ match( goea_result_CC_i$GOID, row.names( added_info_i ) ), ] )
  goea_result_MF_i <- cbind( goea_result_MF_i,
                             added_info_i[ match( goea_result_MF_i$GOID, row.names( added_info_i ) ), ] )
  # Fix column names
  colnames( goea_result_BP_i )[1] <- "GOBPID"
  colnames( goea_result_CC_i )[1] <- "GOCCID"
  colnames( goea_result_MF_i )[1] <- "GOMFID"
  # Output
  write.table( goea_result_BP_i, paste( output_dir_i, "EnrichedGOwithRegulationTypeInfo_BiologicalProcess.tsv", sep="" ), row.names=F, quote=F, sep="\t" )
  write.table( goea_result_CC_i, paste( output_dir_i, "EnrichedGOwithRegulationTypeInfo_CellularComponent.tsv", sep="" ), row.names=F, quote=F, sep="\t" )
  write.table( goea_result_MF_i, paste( output_dir_i, "EnrichedGOwithRegulationTypeInfo_MolecularFunction.tsv", sep="" ), row.names=F, quote=F, sep="\t" )
}
