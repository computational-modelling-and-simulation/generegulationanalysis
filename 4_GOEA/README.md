## About
Perform GO Enrichment analysis for DEGs targeted by transcription factors detected as enriched by LAMP.

## Requirements
- R (v4.0.3)
- [GSEABase](https://bioconductor.org/packages/release/bioc/html/GSEABase.html) (v1.52.1)
- [GOstats](https://bioconductor.org/packages/release/bioc/html/GOstats.html) (v2.56.0)
- [seqinr](https://cran.r-project.org/web/packages/seqinr/index.html) (v4.2-5)
- [GO.db](https://bioconductor.org/packages/release/data/annotation/html/GO.db.html) (v3.12.1)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% R
> if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
> BiocManager::install("GSEABase")
> BiocManager::install("GOstats")
> BiocManager::install("GO.db")
> install.packages("seqinr")
```

## How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("go_enrichment.R")
> source("add_DEG_type_info.R")
> TARGET_SAMPLE <- "NHBE"; source("go_enrichment.R")
> source("add_DEG_type_info.R")
```
