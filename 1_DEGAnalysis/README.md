## About
Extract differentially expressed genes between lung alveolar (A549) cells before and after infection of SARS-CoV2 (GSE147507, https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE147507).
All of the codes were basically same as in https://github.com/saezlab/Covid19_phospho/blob/master/02_Carnival_Transcriptomics_phosphoprotemicsMann.md.

## Requirements
- R (v4.0.3)
- [DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html) (v1.30.0)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% R
> if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
> BiocManager::install("DESeq2")
```

## How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("DEGAnalysis.R")
> TARGET_SAMPLE <- "NHBE"; source("DEGAnalysis.R")
```
