## About
Get the gene regulatory relathionships converted into item file of LAMP (https://github.com/a-terada/lamp) in DoRoThea (https://saezlab.github.io/dorothea/).

## Requirements
- R (v4.0.3)
- [dorothea](http://bioconductor.org/packages/release/data/experiment/html/dorothea.html) (v1.2.2)

For [MacPorts](https://www.macports.org/) users:

```sh
% sudo port install R
% R
> if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
> BiocManager::install("dorothea")
```

## How to use
```sh
% cd SourceCode
% R
> TARGET_SAMPLE <- "A549"; source("main.R")
> TARGET_SAMPLE <- "NHBE"; source("main.R")
```
