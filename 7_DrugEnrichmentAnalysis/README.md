### 7_DrugEnrichmentAnalysis
Extract the enriched target drugs based on INDRA and AILANI database. (See the data in https://gitlab.lcsb.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/harmonised_drugs_mirnas_ups.tsv)

#### Requirements
- R (v4.1.2)
- [openxlsx](https://cran.r-project.org/web/packages/openxlsx/index.html) (v4.2.5)

#### How to use
```sh
% cd 7_DrugEnrichmentAnalysis/SourceCode
% Rscript drug_enrichment_analysis.R
```
