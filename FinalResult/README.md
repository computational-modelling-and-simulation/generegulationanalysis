## Manual Curation
- Revise the DBID of Gemcitabine, Cytarabine
- Eliminate EP-2101 that is not registered in DrugBank
- Eliminate the entry with status not including 'approved"
- Manually added following drugs information
	- MYC
		- Acetaminophen
		- Amsacrine
		- Bortezomib
		- Calcitriol
		- Chloramphenicol
		- Ciprofibrate
		- Curcumin
		- Cyclosporin A
		- Cytarabine
		- Daunorubicin
		- Dexamethasone
		- Docetaxel
		- Doxorubicin
		- Etoposide
		- Everolimus
		- Hyaluronic acid
		- Hydroxyurea
		- imatinib
		- Indomethacin
		- Melphalan
		- Ornithine
		- Paclitaxel
		- Phenobarbital
		- Plicamycin
		- Prednisolone
		- Salicylic acid
		- Tacrolimus
		- Tamoxifen
		- Theophylline
		- Threonine
		- Tyrosine
		- Verapamil
		- Vincristine
		- Vitamin A
		- Zidovudine

	- E2F4
		- Threonine
		- Vitamin C
	- E2F1
		- Chloramphenicol
		- Cisplatin
		- Doxorubicin
		- Estradiol
		- Etoposide
		- Gemcitabine
		- Lovastatin
		- Methotrexate
		- Nitric Oxide
		- Oxygen
		- Paclitaxel
		- Progesterone
		- Tamoxifen
		- Testosterone
		- Tetracycline
		- Vincristine
		- Tyrosine
		- Vitamin C
	- KLF5
		- Zinc
		- Cysteine
	- TP53	
		- Amifostine
		- Bleomycin
		- Bortezomib
		- Caffeine
		- Carboplatin
		- Chlorambucil
		- Curcumin
		- Dicumarol
		- Etoposide
		- Fludarabine
		- Hydroxyurea
		- Irinotecan
		- Lomustine
		- Melphalan
		- Mitomycin
		- Oxaliplatin
		- Paclitaxel
		- Parthenolide
		- Progesterone
		- Sulindac
		- Tamoxifen
		- Temozolomide
		- Topotecan
		- Vinblastine
		- Vincristine
	- AR
		- Adenine
	- MXI1
		- Ornithine
	- TBP
		- Cisplatin
		- Dexamethasone
		- * Formaldehyde (Not Direct Target)
		- * Glycerol (Not Direct Target)
		- Iron
		- Levothyroxine
		- Oxygen
		- Potassium
		- Progesterone
		- Tocopherol
		- Zinc
		- Adenine
		- Cysteine
		- Phenylalanine
		- Vitamin D
	- IRF3
		- Chloramphenicol
		- Zinc
		- Threonine
		- Tyrosine
	- KLF9	
		- Chloramphenicol
		- Progesterone
		- Zinc
	- STAT1
		- Biotin
		- Calcitriol
		- cholesterol
		- Cisplatin
		- Curcumin
		- Cyclosporin A
		- Cysteine
		- Deferoxamine
		- Dexamethasone
		- Doxorubicin
		- Ethanol
		- Etoposide
		- imatinib
		- Imiquimod
		- Indomethacin
		- Nitric Oxide
		- Ornithine
		- Oxygen
		- Phenylalanine
		- Prazosin
		- Progesterone
		- Ribavirin
		- Rosiglitazone
		- Threonine
		- Tyrosine
		- Valsartan
		- Vitamin D
		- Zinc
	- SREBF1
		- Amprenavir
		- Atorvastatin
		- cholesterol
		- Cysteine
		- Estradiol
		- Icosapent
		- Indinavir
		- linoleic acid
		- Lovastatin
		- Nelfinavir
		- oleic acid
		- Ritonavir
		- Testosterone
		- Threonine
		- Tyrosine
	- ATF3
		- Nitric Oxide
		- Oxygen
		- Paclitaxel
		- Testosterone
		- Threonine
		- Troglitazone
		- Tyrosine
	- SP1	
		- Acetylcholine
		- Azathioprine
		- Biotin
		- Calcitriol
		- Celecoxib
		- Chloramphenicol
		- cobalt chloride
		- Curcumin
		- Cysteine
		- Cytarabine
		- Daunorubicin
		- Dexamethasone
		- Dopamine
		- Doxorubicin
		- Epinephrine
		- Estradiol
		- Estrone
		- Etoposide
		- * Formaldehyde (Not Direct Target)
		- Glucosamine
		- Hydroxyurea
		- Metformin
		- Mifepristone
		- Mitoxantrone
		- Morphine
		- N-Acetyl-D-glucosamine
		- Paclitaxel
		- Plicamycin
		- Rifampicin
		- Simvastatin
		- Tamoxifen
		- Testosterone
		- Tetracycline
		- Threonine
		- Troglitazone
		- Tyrosine
		- Valproic acid
		- Vitamin D
		- Zinc 
	- YY1
		- Chloramphenicol
		- cholesterol
		- Cisplatin
		- Morphine
		- Nitric Oxide
		- rituximab
		- Tacrolimus
		- Vitamin D
		- Zinc
	- GABPA
		- Curcumin
		- Oxygen
	- NRF1
		- Caffeine
		- Nitric Oxide
		- Oxygen
	- NR2C2
		- Chloramphenicol
		- Testosterone
	- FOS
		- Amphetamine
		- Apomorphine
		- Calcitriol
		- Capsaicin
		- Cetrorelix
		- Chloramphenicol
		- Clozapine
		- Cocaine
		- Curcumin
		- Dexamethasone
		- Dopamine
		- estradiol benzoate
		-  Haloperidol
		- Levothyroxine
		- Losartan
		- Loxapine
		- Manidipine
		- Morphine
		- Naloxone
		- Olanzapine
		- Ornithine
		- Pilocarpine
		- Remoxipride
		- Tamoxifen
		- Threonine
		- Troglitazone
		- Tyrosine
	- ESR1	
		- Alendronate
		- Aminoglutethimide
		- benzylparaben
		- Bicalutamide
		- Butylparaben
		- Chloramphenicol
		- Cyclophosphamide
		- Dienogest
		- Doxorubicin
		- Flutamide
		- Formestane
		- Gefitinib
		- Ibandronate
		- Methotrexate
		- Methylparaben
		- Mifepristone
		- Paclitaxel
		- Risedronate
		- Teriparatide
		- Testosterone cypionate
		- Testosterone enanthate
		- Testosterone undecanoate
		- Tibolone
		- Toremifene
		- Trastuzumab
		- Vitamin D
		- Zinc
	- RUNX2
		- Alendronate
		- Dexamethasone
		- Fluvastatin
		- Foscarnet
		- Nitric Oxide
		- Oxygen
		- Strontium ranelate
		- Tyrosine
		- Valproic acid
		- Vitamin C
		- Vitamin D
		- Zinc
	- RELA
		- Daunorubicin
		- deoxycholic acid
		- Oxygen
		- Testosterone
	- MYCN
		- Chloramphenicol
		- Cisplatin
		- Dopamine
		- Doxorubicin
		- Etoposide
		- Hydroxyurea
		- Iron
		- Nitric Oxide
		- Ornithine
		- Tetracycline
		- Tyrosine
		- Valproic acid
		- Vincristine
	- BACH1
		- Cysteine
		- Deferoxamine
		- Iron
		- Oxygen
	- FOXP2
		- Nicotinamide
		- Oxygen
		- Tyrosine
	- NFKB1
		- Etoposide
		- Nitric Oxide
	- KLF6
		- Nitric Oxide
		- Zinc	
	- ZNF143  
		- Zinc
	- MYBL2  
		- Chloramphenicol
		- Cyclosporin A
		- Tyrosine
	- FOXO3   
		- Doxorubicin
		- Etoposide
		- imatinib
		- Nitric Oxide
		- Oxygen
		- Testosterone
		- Threonine
		- Tyrosine
	- KLF13
		- Zinc
	- BCL6
		- Cyclophosphamide
		- Cytarabine
		- Doxorubicin
		- imatinib
		- rituximab
		- Vincristine
		- Zinc
		- Tyrosine
	- HIF1A	
		- Bortezomib
		- Capsaicin
		- Carboplatin
		- Celecoxib
		- Cisplatin
		- Cobalt
		- cobalt chloride
		- Cyclophosphamide
		- Cyclosporin A
		- Deferoxamine
		- Dexamethasone
		- Digoxin
		- Docetaxel
		- Dopamine
		- Doxorubicin
		- Doxycycline
		- Etoposide
		- Gemcitabine
		- Halothane
		- Histamine
		- Irinotecan
		- Iron
		- Mannitol
		- Methotrexate
		- Mitomycin
		- Nicotine
		- Nitric Oxide
		- Oxygen
		- Paclitaxel
		- Sunitinib
		- Tacrolimus
		- Tamoxifen
		- Temsirolimus
		- Testosterone
		- Topotecan
		- Trastuzumab
		- Vincristine
		- Asparagine
		- Cysteine
		- Threonine
		- Tyrosine
		- Vitamin C
	- ZEB2
		- Zinc
	- JUN
		- Bexarotene
		- Bortezomib
		- Cerivastatin
		- chenodeoxycholic acid
		- Chloramphenicol
		- Curcumin
		- Cyclosporin A
		- Cytarabine
		- Dexamethasone
		- Dicumarol
		- Etoposide
		- Mifepristone
		- Oxygen
		- Pioglitazone
		- Plicamycin
		- raloxifene
		- Rosiglitazone
		- Tamoxifen
		- Troglitazone
		- Calcitriol
		- Cysteine
		- Threonine
		- Tyrosine
		- Vitamin D
	- ZFX	
		- Zinc

- Manually eliminated following drugs information
	- MBD1 
		- Azacitidine